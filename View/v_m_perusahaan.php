<?php
	include('save_m_perusahaan.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" meta http-equiv="refresh" content="60">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">    
		<!-- Validator -->
    <link rel="stylesheet" href="../dist/css/bootstrapValidator.min.css">  
		<!-- datetimepicker -->
    <link rel="stylesheet" href="../plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css">            
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
				<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Master Perusahaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Administrator Tools</a></li>
            <li class="active">Master Perusahaan</li>
          </ol>
        </section>
        
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Inputan">New</button>
                	<a href="v_m_perusahaan.php" class="btn btn-primary">Refresh</a>
           	                	
                	<!--INPUTAN PERUSAHAAN-->
                	<div id="Inputan" class="modal fade" role="dialog">
									  <div class="modal-dialog">
											<form method="post" class="form-horizontal" data-toggle="validator">
												
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										      	<button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Master Perusahaan</h4>
										      </div>
										      <div class="modal-body">																							      	
									      		<div class="box-body">												      		
									      			
									      			<div class="form-group" has-feedback> <!--No Ref-->
									      				<label for="no_ref" class="col-sm-2 control-label">No Ref</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control"  name="no_ref" placeholder="No Ref">
									      				</div>
									      				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
   															<span class="help-block" with-errors"></span>
									      			</div>
									      			
									      			<div class="form-group"> <!--NAMA PERUSAHAAN-->
									      				<label for="nm_perusahaan" class="col-sm-2 control-label">Nama</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control" id="nm_perusahaan" name="nm_perusahaan" placeholder="Nama Perusahaan">
									      				</div>
									      			</div>
									      		
									      		</div> <!--END OF BOX-BODY-->										      											      
										      </div>									      
										      <div class="modal-footer">									      		
										        <button type="submit" name="insert" class="btn btn-default">Save</button>
										        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										      </div>
										    </div>
											</form>
									  </div>
                	</div>
                	
                	<!--EDIT PERUSAHAAN-->                	                	                	                                	
                	<div id="edit" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<form method="post" class="form-horizontal" data-toggle="validator">
												
												
											<?php $test = $_GET['kd_perusahaan'];
												echo $test;
												
												?>
												
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										      	<button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Master Perusahaan</h4>
										      </div>
										      <div class="modal-body edit-content" >																							      	
									      		<div class="box-body">												      		
									      			
									      			<div class="form-group" has-feedback> <!--No Ref-->
									      				<label for="no_ref" class="col-sm-2 control-label">No Ref</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control"  name="no_ref" placeholder="No Ref" value="<?php echo $row['no_ref'] ?>">
									      				</div>
									      				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
   															<span class="help-block" with-errors"></span>
									      			</div>
									      			
									      			<div class="form-group"> <!--NAMA PERUSAHAAN-->
									      				<label for="nm_perusahaan" class="col-sm-2 control-label">Nama</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control" id="nm_perusahaan" name="nm_perusahaan" placeholder="Nama Perusahaan">
									      				</div>
									      			</div>
									      		
									      		</div> <!--END OF BOX-BODY-->										      											      
										      </div>									      
										      <div class="modal-footer">									      		
										        <button type="submit" name="update" class="btn btn-default">Save</button>
										        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										      </div>
										    </div>
											</form>
									  </div>                		
                	</div>
                	
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="table" class="table table-bordered table-striped table-hover ">
                  	<thead >
                      <tr>                      	
                      	<th style="text-align:center">No.</th>
                      	<th colspan="2" style="text-align:center">Action</th>
                        <th style="text-align:center">No Ref</th>
                        <th style="text-align:center">Nama Perusahaan</th>
                      </tr>
                    </thead>
                  	<tbody>
                  	<?php
                  		include ('../koneksi.php');	
                  		$query = "SELECT  NO_REF,
                  											NM_PERUSAHAAN,
                  											KD_PERUSAHAAN
																FROM M_PERUSAHAAN
																WHERE FLAG_AKTIF = 'T'";
											
											$query_result = $connection->query($query);
											if($query_result->num_rows > 0){
												$row_number = 1;
												while($row = $query_result->fetch_assoc()){
													$kd_perusahaan = $row["KD_PERUSAHAAN"];
													echo '<tr>';
													echo '<td width="10px" style="text-align:center">'.$row_number.'</td>';
													//echo '<td width="10px"><a href="#edit" data-toggle="modal" id="'.$kd_perusahaan.'" data-target="#edit">edit</a></td>';
													echo '<td width="10px"><a href="v_m_perusahaan.php?kd_perusahaan='.$kd_perusahaan.'#edit" data-toggle="modal" >edit</a></td>';
													echo '<td width="10px">delete</td>';													
													echo '<td>'.$row["NO_REF"].'</td>';
													echo '<td>'.$row["NM_PERUSAHAAN"].'</td>';
													echo '</tr>';
													$row_number++;
												}
											}
											$connection->close();
                  	?>
                  	</tbody>
                  </table>
                  <div align="center"><?php echo $information ?></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- date-time-picker -->
    <script src="../../plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>    
    <!--Validator-->
    <script src="../dist/js/validator.min.js"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $('#table').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info":false,
          "autoWidth": true,
          "columnDefs": [{
          	"visible": false
          	}]
        });
        $(".select2").select2();
      });
      
			function reload_page() {
    	location.reload(true);
			}
			
			$(function() {
				//twitter bootstrap script
				$("button#submit-detail").click(function(){
					$.ajax({
						type: "POST",
						url: "process.php",
						data: $('form.inwarditem').serialize(),
						success: function(msg){
						   $("#thanks").html(msg)
						           
						},
							error: function(){
							            alert("failure");	
						}
					});
				});
			});

    </script>
  </body>
</html>
