<?php
	include('save_m_karyawan.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" meta http-equiv="refresh" content="60">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">    
		<!-- Validator -->
    <link rel="stylesheet" href="../dist/css/bootstrapValidator.min.css">  
		<!-- datetimepicker -->
    <link rel="stylesheet" href="../plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css">            
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   
				<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Master Karyawan
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Human Resource</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Karyawan</li>
          </ol>
        </section>
        
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Inputan">New</button>
                	<a href="v_m_karyawan.php" class="btn btn-primary">Refresh</a>
           	                	
                	<!--INPUTAN KARYAWAN-->
                	<div id="Inputan" class="modal fade" role="dialog">
									  <div class="modal-dialog">
											<form method="post" class="form-horizontal" data-toggle="validator">
												
										    <!-- Modal content-->
										    <div class="modal-content">
										      <div class="modal-header">
										      	<button type="button" class="close" data-dismiss="modal">&times;</button>
										        <h4 class="modal-title">Master Karyawan</h4>
										      </div>
										      <div class="modal-body">																							      	
									      		<div class="box-body">												      		
									      			
									      			<div class="form-group" has-feedback> <!--NIK-->
									      				<label for="nik" class="col-sm-2 control-label">NIK</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control"  name="nik" placeholder="No Induk Karyawan" data-error="test" required>
									      				</div>
									      				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
   															<span class="help-block" with-errors"></span>
									      			</div>
									      			
									      			<div class="form-group"> <!--NAMA KARYAWAN-->
									      				<label for="nm_karyawan" class="col-sm-2 control-label">Nama</label>
									      				<div class="col-md-6">
									      					<input type="text" class="form-control" id="nm_karyawan" name="nm_karyawan" placeholder="Nama Karyawan">
									      				</div>
									      			</div>
									      			
									      			<div class="form-group"> <!--BAGIAN-->
									      				<label for="nm_bagian" class="col-sm-2 control-label">Bagian</label>
									      				<div class="col-md-6">
									      					<select class="form-control select2" name="kd_bagian" style="width: 100%;">
									      						<option selected="selected" >-Pilih-</option>
									      						<?php						
									      							include ('../koneksi.php');								      										
									      							$query = "SELECT KD_BAGIAN,NM_BAGIAN FROM M_BAGIAN";
									      							$query_result = $connection->query($query);
									      							if($query_result->num_rows > 0){
									      								while($row = $query_result->fetch_assoc()){									      									
									      									echo '<option value="'.$row["KD_BAGIAN"].'">'.$row["NM_BAGIAN"].'</option>';
									      								}
									      							}
									      							$connection->close();
									      						?>
									      					</select>
									      				</div>
									      			</div>									      			

									      			<div class="form-group"> <!--JABATAN-->
									      				<label for="nm_jabatan" class="col-sm-2 control-label">Jabatan</label>
																<div class="col-md-6">
									      					<select class="form-control select2" name="kd_jabatan" style="width: 100%;">
									      						<option selected="selected">-Pilih-</option>
									      						<?php						
									      							include ('../koneksi.php');								      										
									      							$query = "SELECT KD_JABATAN,NM_JABATAN FROM M_JABATAN";
									      							$query_result = $connection->query($query);
									      							if($query_result->num_rows > 0){
									      								while($row = $query_result->fetch_assoc()){
									      									echo '<option value="'.$row["KD_JABATAN"].'">'.$row["NM_JABATAN"].'</option>';
									      								}
									      							}
									      							$connection->close();
									      						?>
									      					</select>
									      				</div>
									      			</div>

															<div class="form-group"> <!--EMAIL-->
									      				<label for="email" class="col-sm-2 control-label">EMAIL</label>
									      				<div class="col-md-6">
									      					<input type="email" class="form-control" name="email" placeholder="Email">
									      				</div>
									      			</div>
									      										      		
									      			<div class="form-group"> <!--TGL MASUK-->
									      				<label for="tgl_masuk" class="col-sm-2 control-label">Tgl Masuk</label>
									      				<div class="col-md-6">
									      					<div class='input-group date' id='datetimepicker1'>
								                    <input type='text' class="form-control">
								                    <span class="input-group-addon">
								                        <span class="glyphicon glyphicon-calendar"></span>
								                    </span>
								                	</div>
									      				</div>
									      			</div>
									      		
									      		</div> <!--END OF BOX-BODY-->										      											      
										      </div>									      
										      <div class="modal-footer">									      		
										        <button type="submit" name="save" class="btn btn-default">Save</button>
										        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
										        <div align="center"><?php $query ?></div>
										      </div>
										    </div>
											</form>
									  </div>
                	</div>
                	
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="table" class="table table-bordered table-striped">
                  	<thead>
                      <tr>
                      	<th>No.</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Bagian</th>
                        <th>Jabatan</th>
                        <th>Tgl Masuk</th>
                      </tr>
                    </thead>
                  	<tbody>
                  	<?php
                  		include ('../koneksi.php');	
                  		$query = "SELECT 	NIK,
																				NAMA,
																				EMAIL,
																				NM_BAGIAN,
																				NM_JABATAN,
																				TGL_MASUK,
																				m_karyawan.FLAG_AKTIF
																FROM m_karyawan
																		 LEFT JOIN m_bagian
																		 ON m_bagian.KD_BAGIAN = m_karyawan.KD_BAGIAN AND
																				m_bagian.FLAG_AKTIF = 'T'
																		 LEFT JOIN m_jabatan
																		 ON m_jabatan.KD_JABATAN = m_karyawan.KD_JABATAN AND
																				m_jabatan.FLAG_AKTIF = 'T'";
											
											$query_result = $connection->query($query);
											if($query_result->num_rows > 0){
												$row_number = 1;
												while($row = $query_result->fetch_assoc()){
													echo '<tr>';
													echo '<td>'.$row_number.'</td>';
													echo '<td>'.$row["NIK"].'</td>';
													echo '<td>'.$row["NAMA"].'</td>';
													echo '<td>'.$row["EMAIL"].'</td>';
													echo '<td>'.$row["NM_BAGIAN"].'</td>';
													echo '<td>'.$row["NM_JABATAN"].'</td>';
													echo '<td>'.date("d F Y",strtotime($row["TGL_MASUK"])).'</td>';													
													echo '</tr>';
													$row_number++;
												}
											}
											$connection->close();
                  	?>
                  	</tbody>
                  	<div align="center"><?php $query ?></div>
                    <!--<tfoot>
                      <tr>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Bagian</th>
                        <th>Jabatan</th>
                        <th>Tgl Masuk</th>
                      </tr>
                    </tfoot>-->
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <!-- date-time-picker -->
    <script src="../../plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>    
    <!--Validator-->
    <script src="../dist/js/validator.min.js"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $('#table').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info":false,
          "autoWidth": true
        });
        $(".select2").select2();
        $('#datetimepicker1').datetimepicker();
      });
      
			function reload_page() {
    	location.reload(true);
			}
    </script>
  </body>
</html>
